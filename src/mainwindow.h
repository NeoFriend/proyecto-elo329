#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include <QMainWindow>
#include <QPushButton>
#include <QGraphicsRectItem>
#include <QGraphicsScene>
#include <QButtonGroup>
#include <QLineEdit>
#include <QLabel>
#include <QPen>
#include "Pet.h"
#include "User.h"
#include <vector>


QT_BEGIN_NAMESPACE
namespace Ui { class MainWindow; }
QT_END_NAMESPACE

class MainWindow : public QMainWindow
{
    Q_OBJECT

public:
    MainWindow(QWidget *parent = nullptr);
    ~MainWindow();
public slots:
    void updateTextFields(); /**< Funcion a cargo de mantener actualizados los campos de texto de el widget "Check status..." */
    void spawnPet(); /**< Funcion que hace partir el programa haciendo que la mascota aparezca en la escena grafica. */
    void feedPet(); /**< Se llama cuando el boton "Feed..." se clickea, y esta se encarga de llamar el changeHunger de mascota con parametro 1.*/
    void playPet(); /**< Se llama cuando el boton "Play with..." se clickea, y esta se encarga de llamar el changeBoredom de mascota con parametro 1.*/
    void sleepPet(); /**< Se llama cuando el boton "Sleep time..." se clickea, y esta se encarga de llamar el changeSleep de mascota con parametro 1. */
    void medicinePet(); /**< Se llama cuando el boton "Give Medicine..." se clickea, y esta se encarga de llamar el changeHealth de mascota con parametro 1.*/
    void enableMeds(); /**< Esta funcion habilita el boton de medicina, solo cuando la salud de la pet esta en 0. */
    void enableSleep(); /**< Esta funcion habilita el boton de dormir, una vez que la mascota se cansa. */
    void onDeathDisableButtons(); /**< Si es que la mascota muere, se le senala a la UI y esta se preocupa de desactivar todos los botones ya que no se podra hacer nada mas.*/
private:
    Ui::MainWindow *ui;
    User* owner;
    Pet* pet; /**< Puntero a donde se encuentra la mascota del programa.*/
    QGraphicsRectItem *room; /**< "Habitacion" donde se encuentra la mascota, tiene forma de un rectangulo.*/
    QGraphicsScene *place; /**< Escena grafica donde se agregan el resto de elementos visuales como pet, room y mas cosas si se llegaran a implementar. */
    QRandomGenerator myRand;
    QWidget *PetStatus; /**< Widget que tiene campos de textos y labels para mostrarle a la persona con numeros, como se encuentra la mascota, en cuanto salud, hambre, sueno y aburrimiento. */
    QButtonGroup *groupButtons; /**< Un grupo para los botones que Qt nos deja utilizar, para asi poder acceder a los botones despues de crearlos facilmente. */
    std::vector<QLineEdit*>groupLineEdits; /**< Vector para manejar los campos de texto que muestran los estados de la mascota. */
};
#endif // MAINWINDOW_H
