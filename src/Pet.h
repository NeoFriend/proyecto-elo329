#ifndef PET_H
#define PET_H


#include <QTimer>
#include <QString>
#include <QGraphicsRectItem>
#include <QWidget>
#include <QRandomGenerator>
class User;
class Pet: public QWidget
{
    Q_OBJECT
public:
    Pet();
    ~Pet();
    /**
     * @brief eat: Alimenta a la mascota, incrementando el hunger a 1, tambien hay un chance de 1 en 10, de que la mascota se enferme(health a 0) cuando esta se alimenta.
     */
    void eat();
    /**
     * @brief play: Se "entretiene" a la mascota, incrementando su boredom a 1.
     */
    void play();
    /**
     * @brief setName: Implementacion a futuro(la idea era que el usuario pudiera darle a un nombre a la mascota).
     * @param n: String el cual seria el nombre de la mascota.
     */
    void setName(QString n);
    /**
     * @brief setCurrentImage: Funcion la cual se puede llamar de afuera del objeto pet, para asi poder cambiarle la Imagen actual que tiene.
     * @param p: Puntero utilizado para indicarle a la mascota cual sera la Imagen.
     */
    void setCurrentImage(QGraphicsPixmapItem *p);
    /**
     * @brief getHealth: Uno de los getters para revisar los stats de la pet y pasarselos a la GUI.
     * @return: El valor de health de la mascota (0 o 1).
     */
    int getHealth() const;
    /**
     * @brief getHunger: Uno de los getters para revisar los stats de la pet y pasarselos a la GUI.
     * @return: El valor de hunger de la mascota (0 o 1).
     */
    int getHunger() const;
    /**
     * @brief getBoredom: Uno de los getters para revisar los stats de la pet y pasarselos a la GUI.
     * @return: El valor del boredom de la mascota (0 o 1).
     */
    int getBoredom() const;
    /**
     * @brief getSleep: Uno de los getters para revisar los stats de la pet y pasarselos a la GUI.
     * @return: El valor de sleep de la mascota (0 o 1).
     */
    int getSleep() const;
    /**
     * @brief getFTimer: Uno de los getters para acceder a los timers, utilizados mayormente en el mainwindow, para interactuar con los timers desde la GUI.
     * @return : Retorna el timer de la comida, FoodTimer.
     */
    QTimer* getFTimer();
    /**
     * @brief getPTimer: Uno de los getters para acceder a los timers, utilizados mayormente en el mainwindow, para interactuar con los timers desde la GUI.
     * @return : Retorna el timer del aburrimiento, PlayTimer.
     */
    QTimer* getPTimer();
    /**
     * @brief getDTimer: Uno de los getters para acceder a los timers, utilizados mayormente en el mainwindow, para interactuar con los timers desde la GUI.
     * @return : Retorna el timer de muerte, deathTimer.
     */
    QTimer* getDTimer();
    /**
     * @brief getSTimer: Uno de los getters para acceder a los timers, utilizados mayormente en el mainwindow, para interactuar con los timers desde la GUI.
     * @return : Retorna el timer de sleep, sleepTimer.
     */
    QTimer* getSTimer();
    /**
     * @brief getSleeping: Uno de los getters para acceder a los timers, utilizados mayormente en el mainwindow, para interactuar con los timers desde la GUI.
     * @return : Retorna el timer de sleeping.
     */
    QTimer* getSleeping();
    /**
     * @brief getMoveTimer: Uno de los getters para acceder a los timers, utilizados mayormente en el mainwindow, para interactuar con los timers desde la GUI.
     * @return : Retorna el movement Timer.
     */
    QTimer* getMoveTimer();
    QGraphicsRectItem * getView(); /**< Metodo que se ocupaba para acceder a la vista placeholder(rectangulo) desde afuera de la mascota, utilizado anteriormente.*/
    QGraphicsPixmapItem * getImageView(); /**< Metodo actual que se ocupa para acceder a la vista de la mascota y que ahora viene en formato de imagenes png.*/
public slots:
    /**
     * @brief checkAllStates: Funcion que se utiliza para ver como estan los estados de la mascota e actualiza la imagen basado en lo que se leyo.
     */
    void checkAllStates();
    /**
     * @brief changeHealth: Funcion que se utiliza para actualizar el estado de Health.
     * @param newState: Parametro siempre sera 1 o 0.
     */
    void changeHealth(int newState);
    /**
     * @brief changeHunger : Funcion que se utiliza para actualizar el estado de Hunger.
     * @param newState : Parametro siempre sera 1 o 0.
     */
    void changeHunger(int newState=0);
    /**
     * @brief changeBoredom : Funcion que se utiliza para actualizar el estado de Boredom.
     * @param newState : Parametro siempre sera 1 o 0.
     */
    void changeBoredom(int newState=0);
    /**
     * @brief changeSleep : Funcion que se utiliza para actualizar el estado de Sleep.
     * @param newState : Parametro siempre sera 1 o 0.
     */
    void changeSleep(int newState=0);
    void changeToDeath(); /**< Funciona que trabaja en conjunto con deathTimer, esta funcion se llama cuando el timeout ocurre,y se preocupa de cambiar todos los stats de la pet a 0 y cambia la imagen de la pet a una lapida. */
    void simulateMovement(); /**< Funcion que trabaja en conjunto con movementTimer, cuando el timeout ocurre del timer, esta funcion se llama para mover a la pet. */
    void wakingUp(); /**< Este slot es para emitir a la signal wokeUp cuando se necesario. */
signals:
    void stateChanged(); /**< Signal utilizada para indicarle a la GUI que tiene que revisar los estados de la pet para  cambiar la imagen con la cual se muestra la mascota. */
    void gotSick(); /**< Signal especifica para indicarle a la GUI que la health cambio de 1 a 0, por lo tanto habilita el boton de "Give Medicine..." para asi lidiar con el problema. */
    void petDied(); /**< Signal especifica para indicarle a la GUI que la mascota fallecio, por lo tanto se tienen que desactivar las funciones de la pet y los botones. */
    void gotSleepy(); /**<Signal especifica para indicarle a la GUI que la mascota esta con cansancion, y asi habilita el boton "Sleep..." para que la mascota duerma. */
    void wokeUp(); /**< Signal especifica para indicarle a la GUI que la mascota ya no esta durmiendo, y desactiva el boton de "Sleep..." */
private:
    QString name; /**< Nombre de la mascota para implementacion futura. */
    int sleep; /**< 0 needs sleep, 1 no sleep needed. */
    int health; /**< 0 sick, 1 healthy (if 0 for too long then it dies). */
    int hunger; /**< 0 hungry, 1 satisfied (if 0 for too long then it dies). */
    int boredom; /**< 0 bored, 1 content. */
    User *owner; /**< Posible implementacion futura */
    QGraphicsPixmapItem *ImageView; /**< Es el miembro el cual nos permitio ocupar Imagenes png, y poder modificarlas(hacer que se muevan, o cambiar de estado). */
    QGraphicsRectItem *view; /**< Es la vista (en forma de cuadrado) de nuestra pet, esta se utilizo de placeholder para verificar que el codigo estuvier funcionando y luego reemplazamos por varios imagenes png. */
    QTimer *sleeping; /**< Timer utilizado para cuando uno hace dormir a la mascota, esta es incapaz de hacer otras actividades, y se ve "durmiendo" por un tiempo designado por este timer. */
    QTimer *foodTime; /**< Timer utilizado para el hunger de la mascota, una vez que este hace timeout, la mascota se ve con hambre = 0, este solo corre si la hunger !=0. */
    QTimer *playTime; /**< Timer utilizado para el boredom de la mascota, una vez que este hace timeout, la mascota se ve con boredom = 0 (se aburre), este solo corre si boredom != 0. */
    QTimer *sleepTime;/**< Timer utilizado para el sleep de la mascota, una vez que este hace timeout, la mascota se ve con sleep = 0(se cansa), y necesitara dormir, este solo corre si sleep !=0. */
    QTimer *deathTime;/**< Timer utilizado para ver si la pet fallece, este se activa bajo ciertas circunstancias(como hunger=0, health=0), una vez que el timeout ocurre la mascota es incapaz de cualquier otra cosa y "muere" mostrandolo de manera visual con una lapida*/
    QTimer *movementTime;/**< Timer utilizado para simular un movimiento de la pet, asi es lo menos estatica posible. */
    QRandomGenerator myRand;

};

#endif // PET_H
