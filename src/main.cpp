#include "mainwindow.h"

#include <QApplication>
#include <QFile>

int main(int argc, char *argv[])
{
    QApplication a(argc, argv);
    QFile styleSheet("Irrorater.qss");
    styleSheet.open(QFile::ReadOnly);
    QString ss = QLatin1String(styleSheet.readAll());
    a.setStyleSheet(ss);
    MainWindow w;
    w.show();
    return a.exec();
}
