#include "Pet.h"
Pet::Pet(){
    view = new QGraphicsRectItem(NULL);
    deathTime = new QTimer(NULL);
    foodTime = new QTimer(NULL);
    playTime = new QTimer(NULL);
    sleepTime = new QTimer(NULL);
    sleeping = new QTimer(NULL);
    movementTime = new QTimer(NULL);
    myRand = QRandomGenerator::securelySeeded();
    sleep = 1;
    health = 1;
    hunger = 1;
    boredom = 1;

    connect(foodTime,SIGNAL(timeout()),this,SLOT(changeHunger()));
    connect(playTime, SIGNAL(timeout()),this,SLOT(changeBoredom()));
    connect(sleepTime, SIGNAL(timeout()), this, SLOT(changeSleep()));
    connect(movementTime, SIGNAL(timeout()),this,SLOT(simulateMovement()));

}
Pet::~Pet(){
    free(foodTime);
    free(playTime);
    free(sleepTime);
    free(deathTime);
    free(view);
}
void Pet::setCurrentImage(QGraphicsPixmapItem *p){
    ImageView = p;
}
void Pet::checkAllStates(){
    if(health==1 && boredom ==1 && hunger==1 && sleep==1 && !sleeping->isActive()){
        QPixmap pic("happy.png",NULL);
        ImageView->setPixmap(pic.scaled(200,200,Qt::KeepAspectRatio));
    }else if(health==0 && !sleeping->isActive()){
        QPixmap pic("sick.png",NULL);
        ImageView->setPixmap(pic.scaled(200,200,Qt::KeepAspectRatio));
    }else if(hunger==0 && !sleeping->isActive()){
        QPixmap pic("hungry.png",NULL);
        ImageView->setPixmap(pic.scaled(200,200,Qt::KeepAspectRatio));
    }else if(boredom==0 && !sleeping->isActive()){
        QPixmap pic("bored.png",NULL);
        ImageView->setPixmap(pic.scaled(200,200,Qt::KeepAspectRatio));
    }else if(sleep==0 && !sleeping->isActive()){
        QPixmap pic("tired.png",NULL);
        ImageView->setPixmap(pic.scaled(200,200,Qt::KeepAspectRatio));
    }
}
void Pet::changeHealth(int state){
    if(state==1){
        health=1;
        checkAllStates();
        foodTime->start(15000);
        deathTime->stop();
    }else if(state==0){
        health = 0;
        checkAllStates();
        deathTime->start(10000);
        emit gotSick();
    }
    emit stateChanged();
}
void Pet::changeSleep(int state){
    if(state==1){
        sleep=1;
        sleeping->start(5000);
        QPixmap pic("sleepy.png",NULL);
        ImageView->setPixmap(pic.scaled(200,200,Qt::KeepAspectRatio));
        connect(sleeping, SIGNAL(timeout()),sleeping, SLOT(stop()));
        connect(sleeping, SIGNAL(timeout()), this, SLOT(wakingUp()));
        connect(sleeping, SIGNAL(timeout()), this, SLOT(checkAllStates()));
        sleepTime->start(50000);
    }else if(state==0){
        sleep = 0;
        checkAllStates();
        sleepTime->stop();
        emit gotSleepy();
        emit stateChanged();
    }
}
int Pet::getHealth() const{
    return health;
}
int Pet::getHunger() const{
    return hunger;
}
int Pet::getBoredom() const{
    return boredom;
}
int Pet::getSleep() const{
    return sleep;
}
QTimer* Pet::getFTimer(){
    return foodTime;
}
QTimer* Pet::getPTimer(){
    return playTime;
}
QTimer* Pet::getDTimer(){
    return deathTime;
}
QTimer* Pet::getSTimer(){
    return sleepTime;
}
QTimer* Pet::getSleeping(){
    return sleeping;
}
QTimer* Pet::getMoveTimer(){
   return movementTime;
}
QGraphicsRectItem* Pet::getView(){
    return view;
}
QGraphicsPixmapItem * Pet::getImageView(){
    return ImageView;
}
void Pet::changeHunger(int state){
    if(state == 1){
        double c = myRand.generateDouble();
        if(c<=0.1){ //10% chance of getting sick when fed.
            hunger = 1;
            health = 0;
            checkAllStates();
            foodTime->stop();
            deathTime->start(10000);
            emit gotSick();
        }else{
            hunger = 1;
            checkAllStates();
            foodTime->start(10000);
            deathTime->stop();
        }
    }else if(state ==0){
        hunger = 0;
        checkAllStates();
        foodTime->stop();
        deathTime->start(10000);
    }
    emit stateChanged();
    connect(deathTime,SIGNAL(timeout()),this,SLOT(changeToDeath()));
}
void Pet::changeBoredom(int state){
    if(state==1){
        boredom = 1;
        checkAllStates();
        playTime->start(30000);
    }else if(state==0){
        boredom = 0;
        checkAllStates();
        playTime->stop();
    }
    emit stateChanged();
}
void Pet::changeToDeath(){
    QBrush brush(Qt::black);
    view->setBrush(brush);
    hunger= 0;
    health=0;
    sleep=0;
    boredom=0;
    foodTime->stop();
    playTime->stop();
    sleepTime->stop();
    deathTime->stop();
    movementTime->stop();
    QPixmap pic("tombstone.png",NULL);
    ImageView->setPixmap(pic.scaled(200,200,Qt::KeepAspectRatio));
    emit stateChanged();
    emit petDied();
}

void Pet::simulateMovement(){
    int range = 20;
    double rand = myRand.generateDouble();
    if(rand<0.5){
        int num = ImageView->x()+range;
        if(num<280){
            ImageView->setX(num);
        }else{
            ImageView->setX(num-(range*2));
        }
    }else if(rand>0.5){
        int num = ImageView->x()-range;
        if(num>0){
            ImageView->setX(num);
        }else{
            ImageView->setX(num+(range*2));
        }
    }
}

void Pet::wakingUp(){
    emit wokeUp();
}
