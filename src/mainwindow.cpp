#include "mainwindow.h"
#include "ui_mainwindow.h"

MainWindow::MainWindow(QWidget *parent)
    : QMainWindow(parent)
    , ui(new Ui::MainWindow)
{
    ui->setupUi(this);
    setWindowTitle("Neofriend");
    myRand = QRandomGenerator::securelySeeded();

    pet = new Pet();
    room = new QGraphicsRectItem(0,0,450,300);
    QBrush brush(Qt::white);
    QPen pen;
    pen.setColor(Qt::black);
    room->setBrush(brush);
    room->setPen(pen);

    place = new QGraphicsScene(NULL);
    place->addItem(room);
    ui->graphicsView->setScene(place);

    PetStatus = new QWidget(NULL);
    PetStatus->setMinimumSize(400,250);
    QIcon i("leaf.png");
    PetStatus->setWindowIcon(i);
    setWindowIcon(i);
    QWidget* userActions = new QWidget(NULL);
    QPushButton *startDay = new QPushButton("Start day!");
    QPushButton *entertain = new QPushButton("Play with...");
    QPushButton *medicine = new QPushButton("Give Medicine...");
    QPushButton *sleep = new QPushButton("Sleep time...");
    QPushButton *feed = new QPushButton("Feed...");
    QPushButton *checkPet = new QPushButton("Check...");
    checkPet->setDisabled(true);
    groupButtons = new QButtonGroup();
    groupButtons->addButton(startDay,1);
    groupButtons->addButton(entertain,2);
    groupButtons->addButton(feed,3);
    groupButtons->addButton(checkPet,4);
    groupButtons->addButton(medicine,5);
    groupButtons->addButton(sleep, 6);
    groupButtons->button(2)->setDisabled(true);
    groupButtons->button(3)->setDisabled(true);
    groupButtons->button(4)->setDisabled(true);
    groupButtons->button(5)->setDisabled(true);
    groupButtons->button(6)->setDisabled(true);

    QLabel *pHunger = new QLabel("Hunger: ");
    QLabel *pHealth = new QLabel("Health: ");
    QLabel *pSleep = new QLabel("Sleep: ");
    QLabel *pBoredom = new QLabel("Boredom: ");
    QLineEdit * healthTPS = new QLineEdit();
    QLineEdit * hungerTPS = new QLineEdit();
    QLineEdit * boredomTPS = new QLineEdit();
    QLineEdit * sleepTPS = new QLineEdit();
    healthTPS->setReadOnly(true);
    hungerTPS->setReadOnly(true);
    boredomTPS->setReadOnly(true);
    sleepTPS->setReadOnly(true);
    groupLineEdits.push_back(healthTPS);
    groupLineEdits.push_back(hungerTPS);
    groupLineEdits.push_back(boredomTPS);
    groupLineEdits.push_back(sleepTPS);

    QHBoxLayout *petMenu = new QHBoxLayout();
    QVBoxLayout *pStatus = new QVBoxLayout();

    QHBoxLayout *sleepPS = new QHBoxLayout();
    sleepPS->addWidget(pSleep);
    sleepPS->addWidget(sleepTPS);
    QHBoxLayout *hungerPS = new QHBoxLayout();
    hungerPS->addWidget(pHunger);
    hungerPS->addWidget(hungerTPS);
    QHBoxLayout *boredomPS = new QHBoxLayout();
    boredomPS->addWidget(pBoredom);
    boredomPS->addWidget(boredomTPS);
    QHBoxLayout *healthPS = new QHBoxLayout();
    healthPS->addWidget(pHealth);
    healthPS->addWidget(healthTPS);



    petMenu->addWidget(startDay);
    petMenu->addWidget(entertain);
    petMenu->addWidget(feed);
    petMenu->addWidget(sleep);
    petMenu->addWidget(checkPet);
    petMenu->addWidget(medicine);
    userActions->setLayout(petMenu);
    pStatus->addItem(healthPS);
    pStatus->addItem(hungerPS);
    pStatus->addItem(boredomPS);
    pStatus->addItem(sleepPS);
    PetStatus->setLayout(pStatus);
    ui->verticalLayout->addWidget(userActions);

    //PetStatus->show();
    connect(startDay, SIGNAL(pressed()),this,SLOT(spawnPet()));
    connect(feed, SIGNAL(pressed()),this,SLOT(feedPet()));
    connect(entertain, SIGNAL(pressed()),this,SLOT(playPet()));
    connect(checkPet, SIGNAL(pressed()),PetStatus,SLOT(show()));
    connect(sleep,SIGNAL(pressed()),this, SLOT(sleepPet()));
    connect(medicine, SIGNAL(pressed()),this,SLOT(medicinePet()));
    connect(pet, SIGNAL(stateChanged()),this,SLOT(updateTextFields()));
    connect(pet, SIGNAL(gotSick()),this,SLOT(enableMeds()));
    connect(pet, SIGNAL(petDied()), this, SLOT(onDeathDisableButtons()));
    connect(pet, SIGNAL(gotSleepy()), this, SLOT(enableSleep()));
}

MainWindow::~MainWindow()
{
    delete ui;
}
void MainWindow::updateTextFields(){
    groupLineEdits.at(0)->setText(QString::number(pet->getHealth()));
    groupLineEdits.at(1)->setText(QString::number(pet->getHunger()));
    groupLineEdits.at(2)->setText(QString::number(pet->getBoredom()));
    groupLineEdits.at(3)->setText(QString::number(pet->getSleep()));
}
void MainWindow::spawnPet(){
    //pet->getView()->setRect(225,150,50,50);
    //QBrush brush(Qt::green);
    //pet->getView()->setBrush(brush);
    QPixmap pic("happy.png", NULL);
    QGraphicsPixmapItem * p = new QGraphicsPixmapItem(pic.scaled(200,200,Qt::KeepAspectRatio),NULL);
    p->setPos(200,100);
    pet->setCurrentImage(p);
    place->addItem(p);
    pet->getFTimer()->start(15000); //15 Secs until hungry.
    pet->getPTimer()->start(30000); //30 Secs until bored.
    pet->getSTimer()->start(40000); //40 Secs until sleepy.
    pet->getMoveTimer()->start(2000); //Movement timer starts, to simulate movement.
    groupButtons->button(1)->setDisabled(true);
    groupButtons->button(2)->setDisabled(false);
    groupButtons->button(3)->setDisabled(false);
    groupButtons->button(4)->setDisabled(false);
    groupButtons->button(5)->setDisabled(true);
    groupButtons->button(6)->setDisabled(true);
    updateTextFields();

}
void MainWindow::feedPet(){
    pet->changeHunger(1);
}
void MainWindow::playPet(){
    pet->changeBoredom(1);
}
void MainWindow::sleepPet(){
    pet->changeSleep(1);
    groupButtons->button(6)->setDisabled(true);
}
void MainWindow::medicinePet(){
    pet->changeHealth(1);
    groupButtons->button(5)->setDisabled(true);
}
void MainWindow::enableMeds(){
    groupButtons->button(5)->setDisabled(false);
}
void MainWindow::enableSleep(){
    groupButtons->button(6)->setDisabled(false);
}
void MainWindow::onDeathDisableButtons(){
    groupButtons->button(1)->setDisabled(true);
    groupButtons->button(2)->setDisabled(true);
    groupButtons->button(3)->setDisabled(true);
    groupButtons->button(4)->setDisabled(true);
    groupButtons->button(5)->setDisabled(true);
    groupButtons->button(6)->setDisabled(true);
}
