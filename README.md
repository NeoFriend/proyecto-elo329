#### Neofriend:
Este proyecto busca crear una mascota virtual con la cual se quiere fomentar un buen cuidado de las mascotas, en especial hoy en dia que cada vez se está haciendo 
más popular. Esto fue realizado a través del lenguaje de programación C++ en el entorno Qt Creator. Los archivos primarios del código son: 
- main.cpp
- mainwindow.cpp
- mainwindow.h
- mainwindow.ui
- Pet.cpp
- Pet.h
- Tamako.pro
- Tamako.pro.user
- User.cpp
- User.h

Se adjunta algunos archivos extras:
- uml.qmodel
- README.txt

Y algunos archivos que son los recursos gráficos a utilizar para sprites:
- hungry.png
- leaf.png
- sick.png
- sleepy.png
- tired.png
- tombstone.png


#### Instrucciones de compilación en Qt Creator 5.12.11:
1. Abrir Qt Creator
2. Con la configuración mingw apropiada seleccionada (32-bit o 64-bit, dependiendo de la maquina), ingresar a la pestaña "Welcome" a la izquierda de la ventana.
3. Ingresar al tab "Welcome>Projects".
4. Click "Welcome>Projects>Open".
5. Seleccionar el archivo “tamako.pro" en la carpeta del proyecto. 
- Aparecerá un pop-up, ignorarlo y presionar "Ok".
6. Una vez se cargan los archivos, ingresar a la pestaña "Proyects".
7. En la sección "Proyects>Build" seleccionar la carpeta del proyecto como "Build directory".
8. En la sección "Proyects>Run" seleccionar la carpeta del proyecto como "Working directory".
9. Click a la flecha verde en la pestaña izquierda para ejecutar el programa.

#### Consideraciones:
Al partir el programa con el botón “Start day!”, uno debe esperar los timers respectivos para que ocurran cambios que se ven reflejados tanto en la ventana extra que se abre con “Check…” o a través de las distintas apariencias que toma la mascota.
Los tiempos de los timers están con números relativamente cortos para la demostración del funcionamiento de la mascota, para una implementación más oficial, se deberían incrementar los tiempos de estos timers, para simular una mascota de mejor manera.


#### Licencias:
Licencia Qt Theme ocupado “Irrorater.qss”:
Copyright (c) DevSec Studio. All rights reserved.

MIT License

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED *AS IS*, WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.

##### Links a los PNGs ocupados(Flaticon License):
Dragon: Dragon - Free animals icons (flaticon.com)
Tombstone: Tombstone - Free miscellaneous icons (flaticon.com)
Leaf (Window Icon):  https://www.flaticon.com/free-icon/leaf_188333?term=leaf&page=1&position=4&page=1&position=4&related_id=188333&origin=search
